#include <stdlib.h>
#include "item.h"

ContranItem init_item(ContranType type, void* data) {
	ContranItem new_item = { .type = type, .data = data };
	return new_item;
}

void delete_item(ContranItem* item) {
	if ( item == NULL )
		return;
	if ( item->type == WORD )
		free(item->data.word);
		item->data.word = NULL;
}

void print_word(ContranItem* word) {
	if ( word == NULL ) {
		printf("Invalid word.\n");
		return;
	}
	printf("WORD SIZE: %d\n", word[0].data.number);
	int word_size = word[0].data.number;
	for ( int i = 1 ; i < word_size + 1 ; i++ ) {
		print_item(word[i]);
		printf("\n");
	}
}

void print_item(ContranItem item) {
	switch(item.type) {
	case NUMBER:
		printf("TYPE: number VALUE: %d", item.data.number);
		break;
	case STRING:
		printf("TYPE: string VALUE: %s", item.data.string);
		break;
	case IDENTIFIER:
		printf("TYPE: id VALUE: %s", item.data.string);
		break;
	case WORD:
		printf("TYPE: word VALUE: 0x%x", item.data.word);
		break;
	case BUILTIN_ROUTINE:
		printf("TYPE: routine VALUE: 0x%x", item.data.builtin_routine);
		break;
	default:
		printf("UNKNOWN");
	}
}
