#ifndef ITEM_H
#define ITEM_H

#include <stdbool.h>
#include "corth_error.h"

typedef enum ContranType {
	NUMBER,
	STRING,
	IDENTIFIER,
	WORD,
	BUILTIN_ROUTINE,
	ERROR
} ContranType;

typedef struct ContranItem {
	ContranType type;
	union {
		int                 number;
		char*               string;
		struct ContranItem* word;
		void*		    builtin_routine;
		CorthError	    error;

	} data;
} ContranItem;

ContranItem init_item(ContranType type, void* data);
void print_item(ContranItem item);
void delete_item(ContranItem* item);

#endif // ITEM_H
