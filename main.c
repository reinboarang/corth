#include "hashtable.h"
#include "stack.h"
#include "item.h"
#include "primitives.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#define NUMERIC_ALLOWED    "0123456789"
#define IDENTIFIER_ALLOWED "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_+*/?!~.:;<>"

size_t strlcpy(char* dst, const char* src, size_t bufsize) // memory safe strcpy
{
	size_t srclen = strlen(src);
	size_t result = srclen; /* Result is always the length of the src string */
	if(bufsize>0)
	{
		if(srclen>=bufsize)
			srclen=bufsize-1;
		if(srclen>0)
			memcpy(dst,src,srclen);
		dst[srclen]='\0';
	}
	return result;
}

/* name: strcopy_until
 * 
 * src:   source string
 * 
 * dest:  destination string
 *
 * until: position of first occurence of this char determines the length of dest
 *
 * returns: pointer to the start of dest
 */
char* strcopy_until(const char* src, char* dest, char until) {
	int src_len = strlen(src);
	int until_pos = strchrnul(src, until) - (int)src;
	if ( until_pos < 1 || until_pos > src_len ) {
		until_pos = src_len;
	}
	dest = realloc(dest, sizeof(char) * until_pos + 1);
	strlcpy(dest, src, until_pos+1);
	return dest;
}

int tokenize_contran(int len, char* code, char*** result) {
	int result_size = 0;
	char* cur_token = NULL;
	int cur_pos = 0;
	while ( cur_pos < len ) {
		if ( strchr(IDENTIFIER_ALLOWED, code[cur_pos]) || strchr(NUMERIC_ALLOWED, code[cur_pos]) ) {
			cur_token = strcopy_until(&code[cur_pos], cur_token, ' ');
			cur_pos = cur_pos + strlen(cur_token) + 1;
		} else if ( code[cur_pos] == '"' ) {
			cur_token = strcopy_until(&code[cur_pos+1], cur_token, '"');
			int cur_len = strlen(cur_token);
			cur_token = realloc(cur_token, sizeof(char) * (cur_len + 3));
			char* temp_token = strdup(cur_token);
			strcpy(&cur_token[1], temp_token);
			free(temp_token);
			cur_token[0] = '"';
			cur_token[cur_len+1] = '"';
			cur_token[cur_len+2] = '\0';
			cur_pos = cur_pos + strlen(cur_token) + 0;
		} else if ( code[cur_pos] == ' ' ) {
			cur_pos++;
		}

		if ( cur_token ) {
			result_size++;
			*result = realloc(*result, result_size * sizeof(char*));
			(*result)[result_size-1] = cur_token;
			cur_token = NULL;
		}
	}
	return result_size;
}

ContranItem* parse_tokens(int len, char** str) {
	ContranItem* result = malloc(sizeof(ContranItem) * len);
	for ( int i = 0 ; i < len ; i++ ) {
		if ( str[i][0] == '"' ) {
			result[i].type = STRING;
			result[i].data.string = strndup(str[i]+1, strlen(str[i])-2);
		} else if ( strpbrk(str[i], IDENTIFIER_ALLOWED) ) {
			result[i].type = IDENTIFIER;
			result[i].data.string = strdup(str[i]);
		} else if ( strpbrk(str[i], NUMERIC_ALLOWED) ) {
			result[i].type = NUMBER;
			result[i].data.number = atoi(str[i]);
		}
	}
	return result;

}

#define BUILTIN_DEF(key, fnptr, table, itemptr) {\
		itemptr = malloc(sizeof(ContranItem));\
		itemptr->type = BUILTIN_ROUTINE;\
		itemptr->data.builtin_routine = fnptr;\
		set_hash(table, init_entry(key, itemptr));\
	}

HashTable* init_default_def_table() {
	HashTable* def_table = init_hash();
	ContranItem* tmp_item = NULL;

	BUILTIN_DEF("*", builtin_mul, def_table, tmp_item);
	BUILTIN_DEF("+", builtin_add, def_table, tmp_item);
	BUILTIN_DEF("-", builtin_sub, def_table, tmp_item);
	BUILTIN_DEF("dup", dup, def_table, tmp_item);
	BUILTIN_DEF("swap", swap, def_table, tmp_item);
	BUILTIN_DEF(":", colon, def_table, tmp_item);
	BUILTIN_DEF(">r", to_r, def_table, tmp_item);
	BUILTIN_DEF("r>", from_r, def_table, tmp_item);
	BUILTIN_DEF("]", enter_comp_mode, def_table, tmp_item);

	return def_table;
}

HashTable* init_immediate_def_table() {
	HashTable* imm_table = init_hash();
	ContranItem* tmp_item = NULL;

	BUILTIN_DEF(";", semicolon, imm_table, tmp_item);
	BUILTIN_DEF(";;", double_semicolon, imm_table, tmp_item);
	BUILTIN_DEF("[", exit_comp_mode, imm_table, tmp_item);

	return imm_table;
}

ContranItem evaluate_ast(int len, ContranItem* ast, CorthStack* data_stack, CorthStack* exec_stack, CorthStack* return_stack) {
	bool* comp_flag = malloc(sizeof(bool));
	*comp_flag = false;
	HashTable* def_table = init_default_def_table();
	HashTable* imm_table = init_immediate_def_table();
	copy_to_stack(exec_stack, ast, len);
	exec_stack->pos = exec_stack->size - 1;
	while ( exec_stack->pos < exec_stack->size ) {
		if ( *comp_flag ) { // in compile mode
			ContranItem exec_item = pop_stack(exec_stack); // add items from the execute stack into the return stack
			ContranItem* exec_def = NULL;                  // unless we come across an immediate word/routine
			if ( exec_item.type == IDENTIFIER ) {
				HashTableEntry* exec_entry = get_hash(imm_table, exec_item.data.string);

				if ( exec_entry != NULL ) {
					exec_def = exec_entry->value;
				}
				if ( exec_entry == NULL ) {
					push_stack(return_stack, exec_item);
				} else if ( exec_def->type == BUILTIN_ROUTINE ) {
					BuiltInRoutinePtr fun_ptr = exec_def->data.builtin_routine;
					(*fun_ptr)(data_stack, exec_stack, return_stack, def_table, imm_table, comp_flag);
				} else if ( exec_def->type == WORD ) {
					int word_size = exec_item.data.word[0].data.number;
					for ( int i = 1 ; i < word_size + 1 ; i++ ) {
						push_stack(exec_stack, exec_def->data.word[i]);
					}
					break;
				}
			} else {
				push_stack(return_stack, exec_item);
			}
		} else { // not in compile mode
			ContranItem exec_item = pop_stack(exec_stack);
			if ( exec_item.type == NUMBER || exec_item.type == STRING ) {
				push_stack(data_stack, exec_item);
			} else if ( exec_item.type == IDENTIFIER ) {
				HashTableEntry* entry = get_hash(def_table, exec_item.data.string);
				if ( entry != NULL ) {
					BuiltInRoutinePtr fun_ptr;
					ContranItem* entry_item = ((ContranItem*)entry->value);
					int word_size;
					switch ( entry_item->type ) {
					case NUMBER:
					case STRING:
						push_stack(data_stack, *entry_item);
						break;
					case WORD:
						word_size = entry_item->data.word[0].data.number;
						for ( int i = 1 ; i < word_size + 1 ; i++ ) {
							push_stack(exec_stack, entry_item->data.word[i]);
						}
						break;
					case BUILTIN_ROUTINE:
						fun_ptr = entry_item->data.builtin_routine;
						(*fun_ptr)(data_stack, exec_stack, return_stack, def_table, imm_table, comp_flag);
						break;
					}
				}
			} else {
				printf("Unknown item in stack.\n");
			}
		}
		if ( exec_stack->pos == 0 ) {
			return pop_stack(data_stack);
		}
	}
	free(comp_flag);
	delete_hash(def_table);
	delete_hash(imm_table);
	return pop_stack(data_stack);
}

void print_ast(int len, ContranItem* ast) {
	for ( int i = 0 ; i < len ; i++ ) {
		char* i_type;
		switch(ast[i].type) {
		case NUMBER:
			i_type = "NUMBER";
			printf("%d : %s\n", ast[i].data.number, i_type);
			break;
		case STRING:
			i_type = "STRING";
			printf("%s : %s\n", ast[i].data.string, i_type);
			break;
		case IDENTIFIER:
			i_type = "IDENTIFIER";
			printf("%s : %s\n", ast[i].data.string, i_type);
			break;
		case WORD:
			i_type = "WORD";
			printf("0x%x : %s", ast[i].data.word, i_type);
			break;
		default:
			i_type = "UNKNOWN";
			printf("0x%x : %s", ast[i].data.number, i_type);
		}
		printf("\n");
	}
}

int main(int argc, char** argv) {
	if ( argc < 2 ) {
		printf("Code is required.\n");
		return -1;
	}
	printf("CODE: \"%s\"\n", argv[1]);

	char*** con_tokens = malloc(sizeof(char**));
	*con_tokens = malloc(sizeof(char*));

	int len = tokenize_contran(strlen(argv[1]), argv[1], con_tokens);
	ContranItem* items = parse_tokens(len, *con_tokens);

	CorthStack* ds = init_stack(10);
	CorthStack* es = init_stack(len);
	CorthStack* rs = init_stack(10);
	ContranItem result = evaluate_ast(len, items, ds, es, rs);

	print_stack(ds);
	print_ast(1, &result);

	delete_stack(ds);
	delete_stack(es);
	delete_stack(rs);
	return 0;
}
