#include <stdbool.h>
#include "item.h"
#include "stack.h"
#include "primitives.h"
#include "corth_error.h"

void branch(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	es->pos = pop_stack(rs).data.number;
}

void branch_cond(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	
}

void to_r(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	push_stack(rs, pop_stack(ds));
}

void from_r(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	push_stack(ds, pop_stack(rs));
}

void semicolon(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	if ( !*comp_flag ) { // must be in compile mode
		printf("MUST BE IN COMPILE MODE.\n");
		push_stack(ds, init_item(ERROR, COMPILE_MODE_REQUIRED));
		return;
	}
	*comp_flag = false;
	char* word_name = rs->stack[0].data.string;
	shift_stack(rs);
	ContranItem* new_word_def = malloc(sizeof(ContranItem) * (rs->pos+1));
	for ( int i = 0 ; i < rs->pos ; i++ ) {
		new_word_def[i + 1] = rs->stack[rs->pos - i - 1];
	}
	unshift_stack(rs);
	new_word_def[0] = init_item(NUMBER, rs->pos-1);
	ContranItem* new_word = malloc(sizeof(ContranItem));
	*new_word = init_item(WORD, new_word_def);
	set_hash(dict, init_entry(word_name, new_word));
	clear_stack(rs);
}

void double_semicolon(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	semicolon(ds,es,rs,imm_dict,dict,comp_flag);
}

void colon(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	//printf("COMPILE MODE: %s ADDR: 0x%x\n", *comp_flag ? "true" : "false", comp_flag);
	if ( *comp_flag ) { // must not be in compile mode
		push_stack(ds, init_item(ERROR, COMPILE_MODE_FORBIDDEN));
		return;
	}
	*comp_flag = true;
}

void exit_comp_mode(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	*comp_flag = false;
}

void enter_comp_mode(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	*comp_flag = true;
}

void dup(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	push_stack(ds, ds->stack[ds->pos-1]);
}

void swap(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	ContranItem a, b;
	a = pop_stack(ds);
	b = pop_stack(ds);
	push_stack(ds, a);
	push_stack(ds, b);
}

#define BUILTIN_BINARY_OP(op, ds) {\
		int b = pop_stack(ds).data.number;\
		int a = pop_stack(ds).data.number;\
		ContranItem tmp;\
		tmp.type = NUMBER;\
		tmp.data.number = a op b;\
		push_stack(ds, tmp);\
	}

void builtin_sub(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	BUILTIN_BINARY_OP(-,ds);
}

void builtin_add(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	BUILTIN_BINARY_OP(+,ds);
}

void builtin_mul(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag) {
	BUILTIN_BINARY_OP(*,ds);
}

