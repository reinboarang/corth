#include "hashtable.h"

void print_hash(HashTable* ht) {
	printf("SIZE: %d\n", ht->size);
	for ( int i = 0 ; i < ht->size ; i++ ) {
		if ( ht->table[i] ) {
			HashTableEntry* cur_entry = ht->table[i];
			while ( cur_entry ) {
				printf("key: %s\tvalue: %s\thash: %d\n", cur_entry->key, cur_entry->value, generate_hash(cur_entry->key));
				cur_entry = cur_entry->next;
			}
		}
	}
}

int main() {
	HashTable* ht = init_hash();

	set_hash(ht, init_entry("hi", "hello"));
	print_hash(ht);
	set_hash(ht, init_entry("key", "value"));
	print_hash(ht);
	set_hash(ht, init_entry("test", "test value"));
	print_hash(ht);
	set_hash(ht, init_entry("penis", "dick"));
	print_hash(ht);
	
	print_hash(ht);
	printf("VALUE: %s\n", get_hash_char(ht, "hi"));
	delete_hash(ht);
	return 0;
}
