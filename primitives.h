#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include "item.h"
#include "hashtable.h"

typedef void (*BuiltInRoutinePtr)(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void colon(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void semicolon(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void double_semicolon(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag);


void exit_comp_mode(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag);

void enter_comp_mode(CorthStack* ds, CorthStack* es, CorthStack* rs, HashTable* dict, HashTable* imm_dict, bool* comp_flag);

void dup(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void swap(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void to_r(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void from_r(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void builtin_sub(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void builtin_add(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void builtin_mul(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

void builtin_div(CorthStack*, CorthStack*, CorthStack*, HashTable*, HashTable*, bool*);

#endif // PRIMITIVES_H
