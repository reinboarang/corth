#ifndef STACK_H
#define STACK_H

#include "item.h"

typedef struct CorthStack {
	int pos;
	int size;
	ContranItem* stack;
} CorthStack;

CorthStack* init_stack(int size);
void delete_stack(CorthStack* tmp);
void push_stack(CorthStack* tmp, ContranItem item);
ContranItem pop_stack(CorthStack* tmp);
void shift_stack(CorthStack* tmp);
void unshift_stack(CorthStack* tmp); //DO NOT USE WITHOUT FIRST CALLING `shift_stack` !!!
void copy_to_stack(CorthStack* dst, ContranItem* src, int src_len);
ContranItem* copy_from_stack(ContranItem* dst, CorthStack* src);
void clear_stack(CorthStack* stack);
void print_stack(CorthStack* stack);

#endif // STACK_H
