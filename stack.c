#include <stdlib.h>
#include "stack.h"
#include "item.h"

CorthStack* init_stack(int size) {
	CorthStack* tmp = malloc(sizeof(CorthStack));
	tmp->stack = malloc(sizeof(ContranItem) * size);
	tmp->pos = 0;
	tmp->size = size;
	clear_stack(tmp);
	return tmp;
}

void delete_stack(CorthStack* tmp) {
	for ( int i = 0 ; i < tmp->size ; i++ ) {
		delete_item(&tmp->stack[i]);
	}
	free(tmp->stack);
	tmp->stack = NULL;
	free(tmp);
	tmp = NULL;
}

void push_stack(CorthStack* tmp, ContranItem item) {
	tmp->stack[tmp->pos].type = item.type;
	tmp->stack[tmp->pos].data = item.data;
	tmp->pos++;
	if ( tmp->pos >= tmp->size ) {
		tmp->size = tmp->pos + 1;
		tmp->stack = realloc(tmp->stack, sizeof(ContranItem) * tmp->size);
		tmp->stack[tmp->size-1] = init_item(NUMBER, 0);
	}
}

ContranItem pop_stack(CorthStack* tmp) {
	if ( tmp == NULL || tmp->stack == NULL ) {
		return init_item(ERROR, STACK_NULL);
	}
	tmp->pos--;
	if ( tmp->pos < 0 ) {
		ContranItem err;
		err.type = ERROR;
		err.data.error = STACK_UNDERFLOW;
		return err;
	}
	return init_item(tmp->stack[tmp->pos].type, tmp->stack[tmp->pos].data.builtin_routine);
}

void shift_stack(CorthStack* tmp) {
	tmp->stack++;
	tmp->size--;
	tmp->pos--;
}

void unshift_stack(CorthStack* tmp) { //DO NOT USE WITHOUT FIRST CALLING `shift_stack` !!!
	tmp->stack--;
	tmp->size++;
	tmp->pos++;
}

void copy_to_stack(CorthStack* dst, ContranItem* src, int src_len) {
	for ( int i = src_len - 1 ; i >= 0 ; i-- ) {
		push_stack(dst, src[i]);
	}
}

ContranItem* copy_from_stack(ContranItem* dst, CorthStack* src) {
	dst = realloc(dst, src->size * sizeof(ContranItem));
	for ( int i = src->size - 1 ; i >= 0 ; i-- ) {
		dst[i] = src->stack[src->size - 1 - i];
	}
	return dst;
}

void clear_stack(CorthStack* stack) {
	if ( stack == NULL || stack->stack == NULL )
		return;
	for ( int i = 0 ; i < stack->size ; i++ ) {
		stack->stack[i] = init_item(NUMBER,0);
	}
	stack->pos = 0;
}

void print_stack(CorthStack* stack) {
	printf("STACK STRUCTURE\n===============\n");
	for ( int i = 0 ; i < stack->size ; i++ ) {
		print_item(stack->stack[i]);
		if ( i == stack->pos )
			printf("\t<--- %d", i);
		printf("\n");
	}
	printf("\n");
}

